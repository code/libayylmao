/* Require libc printf() */
#include <stdio.h>

/* Translation macros for "ayy" and "lmao" */
#define AYY "ayy"
#define LMAO "lmao"

/* Function prototypes */
void ayy(void);
void lmao(void);
