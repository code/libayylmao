.POSIX:
.PHONY: all install clean

PREFIX = $(HOME)/.local
ALL = libayylmao.a libayylmao.so
OBJ = ayy.o lmao.o

CFLAGS = -fPIC

all: $(ALL)

install: $(ALL)
	mkdir -p -- $(PREFIX)/lib $(PREFIX)/include $(PREFIX)/share/man/man3
	cp -- *.h $(PREFIX)/include
	cp -- *.so *.a $(PREFIX)/lib
	cp -- *.3 $(PREFIX)/share/man/man3

clean:
	rm -f -- $(ALL) $(OBJ)

libayylmao.a: libayylmao.a(ayy.o) libayylmao.a(lmao.o)

libayylmao.so: $(OBJ)
	$(CC) $(CFLAGS) -shared -Wl,-soname,$@ $(LDFLAGS) -o $@ ayy.o lmao.o
